﻿# Equify Books™

 ![Available Version At Primaxlabs](https://i.ibb.co/6sb7k35/equify.jpg)

Equify Books is a cloud based accounting software, open source and online accounting software designed for small businesses and freelancers. It is built with modern technologies such as Laravel, VueJS, Bootstrap 4, RESTful API etc. Thanks to its modular structure, Equify Books provides an awesome App Store for users and developers.

* [Home](https://equifybooks.com) - Home Of Cloud Based Accounting Solution
* [Forum](https://equifybooks.com/forum) - Ask for support
* [Documentation](https://equifybooks.com/docs) - Learn how to use
* [App Store](https://equifybooks.com/apps) - Extend your Equify On Our Market Store

## Requirements

* PHP 7.2 or higher
* Database (eg: MySQL, PostgreSQL, SQLite)
* Web Server (eg: Apache, Nginx, IIS)
* [Other libraries](https://equifybooks.com/docs/requirements)

## Framework

Equify Books uses [Laravel](http://laravel.com), the best existing PHP framework, as the foundation framework

## Installation

* Equify Books can be used by going to our website today [Equify Books](http://equifybooks.com)
* Sign Up `https://equifybooks.com/sign-up`
* Request To Use Equify Books `https://equifybooks.com/request-to-use-equify`
* Accessing Our App Store:

```bash
On your profile click on => `get-equify-access-token` to generate our access token, to get access to the equify Store
```

## Full Ownership and License, Credits to the developer
Software and its core functions and features are fully liscenced and owned by primaxlab and its authorities thereof, no copy or distribution of the exact copy verbatim is allowed, Equify is built by [Mathemartins](https://github.com/mathemartins), with inspiration from akaunting....
`Note: This is not an open sourced project` 

## Backlog
We are building our equify 2.0

## Contact Us
Please contact the developer on mathegeniuse@gmail.com