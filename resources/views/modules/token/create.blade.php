@extends('layouts.modules')

@section('title', trans('modules.title'))

@section('content')
    <div class="box box-success">
        <div class="well-sm">
            <h2 class="text-center text-capitalize">Welcome to Equify Market Store!</h2>
        </div>
    </div>
@endsection