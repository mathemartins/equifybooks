<?php

return [

    'installed_version'     => 'Εγκατεστημένη έκδοση',
    'latest_version'        => 'Τελευταία έκδοση',
    'update'                => 'Ενημέρωση Equify Books στην έκδοση :version',
    'changelog'             => 'Αρχείο καταγραφής αλλαγών',
    'check'                 => 'Έλεγχος',
    'new_core'              => 'Διατίθεται μια ενημερωμένη έκδοση του Equify Books.',
    'latest_core'           => 'Συγχαρητήρια! Έχετε την τελευταία έκδοση του Equify Books. Οι μελλοντικές ενημερώσεις ασφαλείας θα εφαρμόζονται αυτόματα.',
    'success'               => 'Η ενημέρωση έχει ολοκληρωθεί επιτυχώς.',
    'error'                 => 'Η ενημέρωση απέτυχε. Παρακαλώ προσπαθήστε ξανά.',

];
