<?php

return [

    'installed_version'     => 'Versija',
    'latest_version'        => 'Naujausia versija',
    'update'                => 'Atnaujinti Equify Books į :version versiją',
    'changelog'             => 'Pakeitimų sąrašas',
    'check'                 => 'Tikrinti',
    'new_core'              => 'Yra naujesnė Equify Books versija.',
    'latest_core'           => 'Sveikiname! Jūs turite naujausią Equify Books versiją. Tolimesni atnaujinimai bus įrašomi automatiškai.',
    'success'               => 'Atnaujinimas pavyko sėkmingai.',
    'error'                 => 'Įvyko klaida atnaujinant, prašome bandyti dar kartą.',

];
