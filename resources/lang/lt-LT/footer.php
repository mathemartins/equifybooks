<?php

return [

    'version'               => 'Versija',
    'powered'               => 'Sukurta Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Nemokama apskaitos programa',

];
