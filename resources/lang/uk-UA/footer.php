<?php

return [

    'version'               => 'Версія',
    'powered'               => 'Зроблено в Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Безкоштовна Бухгалтерська Програма',

];
