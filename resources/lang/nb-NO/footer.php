<?php

return [

    'version'               => 'Versjon',
    'powered'               => 'Drevet med Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Gratis regnskapsprogram',

];
