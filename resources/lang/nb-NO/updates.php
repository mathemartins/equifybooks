<?php

return [

    'installed_version'     => 'Installert versjon',
    'latest_version'        => 'Nyeste versjon',
    'update'                => 'Oppdater Equify Books til :version',
    'changelog'             => 'Endringslogg',
    'check'                 => 'Se etter oppdatering',
    'new_core'              => 'Det finnes en oppdatert versjon av Equify Books.',
    'latest_core'           => 'Gratulerer! Du har den siste versjonen av Equify Books.',
    'success'               => 'Oppdateringsprosessen er fullført.',
    'error'                 => 'Oppdateringsprosessen feilet. Forsøk igjen.',

];
