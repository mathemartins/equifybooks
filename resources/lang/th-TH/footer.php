<?php

return [

    'version'               => 'เวอร์ชัน',
    'powered'               => 'ขับเคลื่อน โดย Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'ซอฟต์แวร์บัญชีฟรี',

];
