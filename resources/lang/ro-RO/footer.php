<?php

return [

    'version'               => 'Versiunea',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Program de contabilitate gratuit',

];
