<?php

return [

    'version'               => 'Верзија',
    'powered'               => 'Овозможенo од Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Бесплатен сметководствен софтвер',

];
