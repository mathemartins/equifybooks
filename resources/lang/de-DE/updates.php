<?php

return [

    'installed_version'     => 'Installierte Version',
    'latest_version'        => 'Neueste Version',
    'update'                => 'Equify Books auf Version :version updaten',
    'changelog'             => 'Changelog',
    'check'                 => 'Prüfen',
    'new_core'              => 'Eine aktualisierte Version von Equify Books ist verfügbar.',
    'latest_core'           => 'Glückwunsch! Sie nutzen die aktuellste Version von Equify Books. Zukünftige Sicherheitsupdates werden automatisch angewendet.',
    'success'               => 'Der Updateprozess wurde erfolgreich ausgeführt.',
    'error'                 => 'Updateprozess fehlgeschlagen, bitte erneut versuchen.',

];
