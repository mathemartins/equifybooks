<?php

return [

    'version'               => 'Version',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Kostenlose Buchhaltungssoftware',

];
