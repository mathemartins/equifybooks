<?php

return [

    'version'               => 'Verzija',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Slobodan računovodstveni softver',

];
