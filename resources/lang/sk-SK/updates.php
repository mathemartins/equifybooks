<?php

return [

    'installed_version'     => 'Nainštalovaná verzia',
    'latest_version'        => 'Najnovšia verzia',
    'update'                => 'Aktualizácia Equify Books-u na :version verziu',
    'changelog'             => 'Zoznam zmien',
    'check'                 => 'Skontrolovať',
    'new_core'              => 'K dispozícii je aktualizovaná verzia z Equify Books.',
    'latest_core'           => 'Gratulujem! Máte najnovšiu verziu Equify Books. Budúce aktualizácie zabezpečenia sa použije automaticky.',
    'success'               => 'Proces aktualizácie bol úspešne dokončený.',
    'error'                 => 'Proces aktualizácie zlyhal, prosím, skúste to znova.',

];
