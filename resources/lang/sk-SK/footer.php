<?php

return [

    'version'               => 'Verzia',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Zadarmo účtovný softvér',

];
