<?php

return [

    'version'               => 'نسخه',
    'powered'               => 'طراحی شده توسط Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'نرم افزار حسابداری رایگان',

];
