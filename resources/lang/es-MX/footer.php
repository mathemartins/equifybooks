<?php

return [

    'version'               => 'Versión',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Software de Contabilidad Libre',

];
