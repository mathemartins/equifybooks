<?php

return [

    'installed_version'     => 'Versão Instalada',
    'latest_version'        => 'Última Versão',
    'update'                => 'Atualiza o Equify Books para a versão :version',
    'changelog'             => 'Changelog',
    'check'                 => 'Verificar',
    'new_core'              => 'Está disponível uma versão atualizada do Sistema Equify Books.',
    'latest_core'           => 'Parabéns! Você tem a versão mais recente do Equify Books. Futuras atualizações de segurança serão aplicadas automaticamente.',
    'success'               => 'A instalação foi completada com êxito.',
    'error'                 => 'Houve um erro na atualização! Por favor tente novamente.',

];
