<?php

return [

    'version'               => 'Versão',
    'powered'               => 'Desenvolvido por Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Software de contabilidade gratuito',

];
