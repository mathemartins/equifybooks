<?php

return [

    'version'               => 'Versija',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Bezmaksas grāmatvedības programma',

];
