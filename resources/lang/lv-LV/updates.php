<?php

return [

    'installed_version'     => 'Instalēta versija',
    'latest_version'        => 'Pēdējā versija',
    'update'                => 'Atjaunināt Equify Books uz :version versiju',
    'changelog'             => 'Izmaiņas',
    'check'                 => 'Pārbaudīt',
    'new_core'              => 'Ir pieejama Equify Books atjaunināta versija.',
    'latest_core'           => 'Apsveicu! Jums ir jaunākā Equify Books versija. Nākamos drošības uzlabojumus jūs saņemsiet automātiski.',
    'success'               => 'Atjaunināšanas process ir veiksmīgi pabeigts.',
    'error'                 => 'Atjaunināšanas procesā notika kļūda, mēģiniet vēlreiz.',

];
