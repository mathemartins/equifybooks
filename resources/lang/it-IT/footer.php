<?php

return [

    'version'               => 'Versione',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'A Primaxlab Accounting Software',

];
