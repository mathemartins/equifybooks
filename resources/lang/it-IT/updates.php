<?php

return [

    'installed_version'     => 'Versione installata',
    'latest_version'        => 'Ultima versione',
    'update'                => 'Aggiornamento Equify Books alla versione :version',
    'changelog'             => 'Modifiche di Versione',
    'check'                 => 'Controlla',
    'new_core'              => 'È disponibile una versione aggiornata di Equify Books.',
    'latest_core'           => 'Congratulazioni! Hai l\'ultima versione di Equify Books. Aggiornamenti di sicurezza per il futuro verranno applicati automaticamente.',
    'success'               => 'Installazione completata con successo.',
    'error'                 => 'Processo di aggiornamento non è riuscito, per favore, riprova.',

];
