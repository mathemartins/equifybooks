<?php

return [

    'version'               => 'Versioni',
    'powered'               => 'Mundësuar nga Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Program Kontabiliteti Falas',

];
