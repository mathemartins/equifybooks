<?php

return [

    'version'               => 'إصدار',
    'powered'               => 'بواسطة أكاونتينج',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'برنامج محاسبي مجاني',

];
