<?php

return [

    'installed_version'     => 'Nainstalovaná verze',
    'latest_version'        => 'Nejnovější verze',
    'update'                => 'Aktualizovat Equify Books na verzi :version',
    'changelog'             => 'Seznam změn',
    'check'                 => 'Zkontrolovat',
    'new_core'              => 'K dispozici je aktualizovaná verze Equify Books.',
    'latest_core'           => 'Blahopřejeme! Máte nejnovější verzi Equify Books. Budoucí aktualizace budou nainstalovány automaticky.',
    'success'               => 'Proces aktualizace byl úspěšně dokončen.',
    'error'                 => 'Proces aktualizace se nezdařil, prosím, zkuste to znovu.',

];
