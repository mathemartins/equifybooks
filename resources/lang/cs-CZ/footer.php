<?php

return [

    'version'               => 'Verze',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Účetní software zdarma',

];
