<?php

return [

    'installed_version'     => 'Installeret version',
    'latest_version'        => 'Seneste version',
    'update'                => 'Opdatere Equify Books til :version version',
    'changelog'             => 'Ændringslog',
    'check'                 => 'Kontrollér',
    'new_core'              => 'Der findes en opdateret version af Equify Books.',
    'latest_core'           => 'Tillykke! Du har nu den nyeste version af Equify Books. Fremtidige sikkerhedsopdateringer installeres automatisk.',
    'success'               => 'Opdateringen er gennemført.',
    'error'                 => 'Opdateringen er fejlet, prøv igen.',

];
