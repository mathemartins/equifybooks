<?php

return [

    'version'               => 'Version',
    'powered'               => 'Drevet af Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Gratis regnskabsprogram',

];
