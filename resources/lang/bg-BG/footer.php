<?php

return [

    'version'               => 'Версия',
    'powered'               => 'С подкрепата на Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Безплатен счетоводен софтуер',

];
