<?php

return [

    'version'               => 'ورژن',
    'powered'               => 'اکاونٹانگ کی طرف سے حقوق محفوظ ہیں',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'مفت اکاؤنٹنگ سافٹ ویئر',

];
