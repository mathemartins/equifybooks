<?php

return [

    'version'               => 'ვერსია',
    'powered'               => 'მუშაობს აქაუტინგზე',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'უფასო საბუღალტრო პროგრამა',

];
