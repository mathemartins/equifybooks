<?php

return [

    'version'               => 'Versie',
    'powered'               => 'Mogelijk gemaakt door Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Gratis boekhoudsoftware',

];
