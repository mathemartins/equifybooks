<?php

return [

    'installed_version'     => 'Geïnstalleerde versie',
    'latest_version'        => 'Laatste versie',
    'update'                => 'Equify Books bijwerken naar versie :version',
    'changelog'             => 'Wijzigingslogboek',
    'check'                 => 'Controleren',
    'new_core'              => 'Er is een nieuwere versie van Equify Books beschikbaar.',
    'latest_core'           => 'Gefeliciteerd! U heeft de laatste versie van Equify Books. Toekomstige beveiligingsupdates zullen automatisch worden toegepast.',
    'success'               => 'Update-proces is voltooid.',
    'error'                 => 'Update-proces is mislukt. Probeer het alstublieft opnieuw.',

];
