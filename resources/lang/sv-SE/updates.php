<?php

return [

    'installed_version'     => 'Installerad version',
    'latest_version'        => 'Senaste versionen',
    'update'                => 'Uppdatera Equify Books till version :version',
    'changelog'             => 'Ändringslog',
    'check'                 => 'Markera',
    'new_core'              => 'Det finns en uppdaterad version av Equify Books.',
    'latest_core'           => 'Grattis! Du har den senaste versionen av Equify Books. Framtida säkerhetsuppdateringar kommer att tillämpas automatiskt.',
    'success'               => 'Uppdateringen har fullföljts.',
    'error'                 => 'Uppdateringen har misslyckats, var vänlig, försök igen.',

];
