<?php

return [

    'version'               => 'Versi',
    'powered'               => 'Didukung oleh Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Perangkat Lunak Akutansi Gratis',

];
