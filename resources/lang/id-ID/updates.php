<?php

return [

    'installed_version'     => 'Versi Terpasang',
    'latest_version'        => 'Revisi Terakhir',
    'update'                => 'Memperbaharui Equify Books ke versi :version',
    'changelog'             => 'Catatan Perubahan',
    'check'                 => 'Periksa',
    'new_core'              => 'Versi terbaru Equify Books tersedia.',
    'latest_core'           => 'Selamat! Anda telah memiliki revisi terbaru Equify Books. Pembaharuan keamanan untuk kedepan akan diterapkan secara otomatis.',
    'success'               => 'Proses pembaharuan telah selesai dengan baik.',
    'error'                 => 'Proses pembaharuan gagal, silakan, coba lagi.',

];
