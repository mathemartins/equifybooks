<?php

return [

    'version'               => 'Version',
    'powered'               => 'Propulsé par Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Logiciel de comptabilité gratuit',

];
