<?php

return [

    'version'               => 'גירסה',
    'powered'               => 'מופעל על ידי Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'תוכנת הנהלת חשבונות חינם',

];
