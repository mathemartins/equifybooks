<?php

return [

    'version'               => 'Phiên bản',
    'powered'               => 'Powered By Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Phần mềm kế toán miễn phí',

];
