<?php

return [

    'version'               => 'Верзија',
    'powered'               => 'Омогућио вам је Equify Books',
    'link'                  => 'https://equifybooks.com',
    'software'              => 'Слободан рачуноводствени програм',

];
